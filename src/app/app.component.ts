import { Component, ElementRef, ViewChild } from '@angular/core';

import Map from 'ol/Map';
import View from 'ol/View';
import VectorLayer from 'ol/layer/Vector';
import Icon from 'ol/style/Icon';
import OSM from 'ol/source/OSM';
import * as olProj from 'ol/proj';
import TileLayer from 'ol/layer/Tile';
import VectorSource from 'ol/source/Vector';
import {Fill, Stroke, Style, Text} from 'ol/style';
import GeoJSON from 'ol/format/GeoJSON';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  map : any;  // variable for storing map

  @ViewChild('info',{static:false})info :ElementRef
  @ViewChild('tla',{static:false})tla :ElementRef

  style = new Style({
    fill: new Fill({
      color: '#F0F0F0',
    }),
    stroke: new Stroke({
      color: 'white',
      width: 1,
    }),
    text: new Text({
      font: '12px Calibri,sans-serif',
      fill: new Fill({
        color: '#F0F0F0',
      }),
      stroke: new Stroke({
        color: '#F0F0F0',
        width: 0,
      }),
    }),
  });

  vectorLayer = new VectorLayer({
    source: new VectorSource({
      url: 'assets/countries.geojson',
      format: new GeoJSON(),
    }),
    style: (feature) => {
      this.style.getText().setText(feature.get('name'));
      return this.style;
    },
  });

  highlight;

  highlightStyle = new Style({
    stroke: new Stroke({
      color: '#1D8D84',
      width: 1,
    }),
    fill: new Fill({
      color: '#1D8D84',
    }),
    text: new Text({
      font: '12px Calibri,sans-serif',
      fill: new Fill({
        color: '#1D8D84',
      }),
      stroke: new Stroke({
        color: '#1D8D84',
        width: 0,
      }),
    }),
  });
  featureOverlay: any;


ngOnInit(){
  this.map = new Map({
    layers: [this.vectorLayer],
    target: 'hotel_map',
    view: new View({
      center: [0, 0],
      zoom: 0,
    }),
  }); 
}

// ngAfterViewInit(){
//   this.featureOverlay = new VectorLayer({
//     source: new VectorSource(),
//     map: this.map,
//     style:  (feature)=> {
//       this.highlightStyle.getText().setText(feature.get('name'));
//       return this.highlightStyle;
//     },
//   }); ;

//   new VectorLayer({
//     source: new VectorSource(),
//     map: this.map,
//     style:  (feature)=> {
//       this.highlightStyle.getText().setText(feature.get('name'));
//       return this.highlightStyle;
//     },
//   });

//   let countryCode = 'IND'
//   if (countryCode.length > 2) {
//     let feature = this.vectorLayer.getSource().getFeatureById(countryCode);
//     if (this.highlight) {
//         this.featureOverlay.getSource().removeFeature(this.highlight);
//     }
//     if (feature) {
//         this.featureOverlay.getSource().addFeature(feature);
//     }
//     this.highlight = feature;
// }

//   // let displayFeatureInfo =  (pixel)=> {
//   //   let feature = this.map.forEachFeatureAtPixel(pixel,(feature)=> {
//   //     return feature;
//   //   });

//   //   // let info = document.getElementById('info');
//   //   if (feature) {
//   //     this.info.nativeElement.innerHTML = feature.getId() + ': ' + feature.get('name');
//   //   } else {
//   //     this.info.nativeElement.innerHTML = '&nbsp;';
//   //   }

//   //   if (feature !== this.highlight) {
//   //     if (this.highlight) {
//   //       this.featureOverlay.getSource().removeFeature(this.highlight);
//   //     }
//   //     if (feature) {
//   //       this.featureOverlay.getSource().addFeature(feature);
//   //     }
//   //     this.highlight = feature;
//   //   }
//   // };

//   // this.map.on('pointermove',  (evt)=> {
//     // if (evt.dragging) {
//     //   return;
//     // }
//     // let pixel = this.map.getEventPixel(evt.originalEvent);
//     // displayFeatureInfo(pixel);
//   // });

//   // this.map.on('click', (evt)=> {
//   //   if (evt.dragging) {
//   //     return;
//   //   }
//   //   let pixel = this.map.getEventPixel(evt.originalEvent);
//   //   displayFeatureInfo(pixel);
//   //   // displayFeatureInfo(evt.pixel);
//   // });
// }

ngAfterViewInit(){
  this.featureOverlay = new VectorLayer ({
    source: new VectorSource(),
    map: this.map,
    style: (feature)=> {
      this.highlightStyle.getText().setText(feature.get('name'));
      return this.highlightStyle;
    }
  });
  
  
  this.tla.nativeElement.oninput=()=> {
      if (this.tla.nativeElement.value.length > 2) {
          var feature = this.vectorLayer.getSource().getFeatureById(this.tla.nativeElement.value);
          if (this.highlight) {
            this.featureOverlay.getSource().removeFeature(this.highlight);
          }
          if (feature) {
            this.featureOverlay.getSource().addFeature(feature);
            let extent = this.featureOverlay.getSource().forEachFeature(feature=>{
              return feature.getGeometry().getExtent()
              });
              // let zoomBounds =  transformExtent(this.zoomBounds,'EPSG:4326','EPSG:3857');
              this.map.getView().fit(extent,(this.map.getSize()));
              this.map.getView().setZoom(0);
              setTimeout( ()=> {
                this.map.updateSize();
              }, 200);
            // this.map.getView().fitExtent(extent,this.map.getSize())
          }
          this.highlight = feature;
      }
  };
}


}

